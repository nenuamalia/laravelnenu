@extends('layouts.plain')

@section('content')
<div class="app-container">
    <div class="h-100">
        <div class="h-100 no-gutters row">
            <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-12">
                <div class="mx-auto app-login-box col-sm-12 col-md-8 col-lg-6">
                    <div class="app-logo"></div>
                    <h4>
                        <div>Forgot your Password?</div>
                        <span>Use the form below to recover it.</span>
                    </h4>
                    <div>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="position-relative form-group">
                                        <label for="email" class="">{{ __('E-Mail Address') }}</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email here..." required autocomplete="email" autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 d-flex align-items-center">
                                <h6 class="mb-0">
                                    <a href="{{ route('login') }}" class="text-primary">Sign in existing account</a>
                                </h6>
                                <div class="ml-auto">
                                    <button class="btn btn-primary btn-lg">Recover Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection