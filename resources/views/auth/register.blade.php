@extends('layouts.plain')

@section('content')
<div class="app-container">
    <div class="h-100">
        <div class="h-100 no-gutters row">
            <div
                class="h-100 d-md-flex d-sm-block bg-white justify-content-center align-items-center col-md-12 col-lg-12">
                <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                    <div class="app-logo"></div>
                    <h4>
                        <div>Welcome,</div>
                        <span>It only takes a <span class="text-success">few seconds</span> to create your account</span>
                    </h4>
                    <div>
                        <form method="POST" action="{{ route('register') }}" autocomplete="off">
                            @csrf

                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="exampleEmail" class=""><span class="text-danger">*</span> {{ __('E-Mail Address') }}</label>
                                        <input name="email" id="email" placeholder="Email here..." type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="exampleNoHp" class=""><span class="text-danger">*</span> {{ __('No Handphone') }}</label>
                                        <input id="nohp" type="text" class="form-control @error('nohp') is-invalid @enderror" name="nohp" value="{{ old('nohp') }}" placeholder="No Handphone here..." required autocomplete="nohp" autofocus onkeyup="checkonlynumber(this);">

                                        @error('nohp')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="examplePassword" class=""><span class="text-danger">*</span> {{ __('Password') }}</label>
                                        <input name="password" id="password" placeholder="Password here..." type="password" class="form-control @error('password') is-invalid @enderror" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="examplePasswordRep" class=""><span class="text-danger">*</span> {{ __('Repeat Password') }}</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Repeat Password here..." required autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="exampleAlamat" class="">{{ __('Alamat') }}</label>
                                        <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="10">{{ old('alamat') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 d-flex align-items-center">
                                <h5 class="mb-0">Already have an account? <a href="{{ route('login') }}" class="text-primary">Sign in</a></h5>
                                <div class="ml-auto">
                                    <button class="btn-wide btn-pill btn-shadow btn-hover-shine btn btn-primary btn-lg">Create Account </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection