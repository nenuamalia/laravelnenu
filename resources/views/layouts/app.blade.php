
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Wide selection of buttons that feature different styles for backgrounds, borders and hover options!">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Styles -->
    <link href="{{ asset('public/architectui/main.css') }}" rel="stylesheet">
    <link href="{{ asset('public/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('public/plugins/jQuery/jquery-3.5.0.min.js') }}" defer></script>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
        <div class="app-header header-shadow bg-alternate header-text-light">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="{{ asset('public/architectui/assets/images/avatars/no-avatar.png') }}" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                            <div class="dropdown-menu-header">
                                                <div class="dropdown-menu-header-inner bg-alternate">
                                                    <div class="menu-header-content text-left">
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left mr-3">
                                                                    <img width="42" class="rounded-circle" src="{{ asset('public/architectui/assets/images/avatars/no-avatar.png') }}" alt="">
                                                                </div>
                                                                <div class="widget-content-left">
                                                                   <div class="widget-heading">{{ Auth::user()->email }}</div>
                                                                </div>
                                                                <div class="widget-content-right mr-2">
                                                                    <a href="{{ route('logout') }}" type="button" tabindex="0" class="btn-pill btn-shadow btn-shine btn btn-focus" onclick="event.preventDefault();
                                                                        document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

                                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                    @csrf
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading"> {{ Auth::user()->email }} </div>
                                </div>
                            </div>
                        </div>
                    </div>       
                </div>
            </div>
        </div>        
        <div class="app-main">
                @include('layouts.sidebar')
                <div class="app-main__outer">
                    @yield('content')
                </div>
        </div>
    </div>
    <div class="app-drawer-overlay d-none animated fadeIn"></div>
    
    <!-- Scripts -->
    <script src="{{ asset('public/architectui/assets/scripts/main.js') }}" defer></script>
    <script src="{{ asset('public/js/global.js') }}" defer></script>
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" defer></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap4.min.js') }}" defer></script>
    <script src="{{ asset('public/plugins/PriceFormat/jquery.price_format.1.8.min.js') }}" defer></script>
    <script src="{{ asset('public/plugins/jQueryForm/jquery.form.js') }}" defer></script>

    @stack('scripts')
</body>

</html>