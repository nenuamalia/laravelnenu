<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <!-- Styles -->
    <link href="{{ asset('public/architectui/main.css') }}" rel="stylesheet">

<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        @yield('content')
    </div>

    
    <!-- Scripts -->
    <script src="{{ asset('public/architectui/assets/scripts/main.js') }}" defer></script>
    <script src="{{ asset('public/plugins/jQuery/jquery-3.5.0.min.js') }}" defer></script>
    <script src="{{ asset('public/js/global.js') }}" defer></script>
    <script src="{{ asset('public/plugins/PriceFormat/jquery.price_format.1.8.min.js') }}" defer></script>
</html>