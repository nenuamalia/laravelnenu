@extends('layouts.app')

@section('content')
<div class="app-main__inner">
    <div class="card-header">
        <i class="header-icon pe-7s-magic-wand icon-gradient bg-tempting-azure"> </i> {{ $title }}
    </div>             
    <div class="main-card mb-3 card">
        <div class="card-body">
            <form class="">
                <div class="position-relative row form-group"><label for="exampleEmail" class="col-sm-3 col-form-label">Nama Produk</label>
                    <div class="col-sm-9"><label class="col-sm-12 col-form-label">: {{ $res->nama_produk }} ?></label></div>
                </div>
                <div class="position-relative row form-group"><label for="exampleEmail" class="col-sm-3 col-form-label">Harga</label>
                    <div class="col-sm-9"><label class="col-sm-12 col-form-label">: {{  number_format($res->harga, 0, ',', '.') }}</label></div>
                </div>
                <div class="position-relative row form-group"><label for="exampleEmail" class="col-sm-3 col-form-label">Stock</label>
                    <div class="col-sm-9"><label class="col-sm-12 col-form-label">: {{  number_format($res->stock, 0, ',', '.') }}</label></div>
                </div>
                <div class="position-relative row form-group"><label for="exampleEmail" class="col-sm-3 col-form-label">Gambar Produk</label>
                    <div class="col-sm-9">
                        <img src='{{ asset("public/images/produk/{$res->image}") }}' alt='user' width='auto' height='100' />
                    </div>
                </div>
            </form>
             <button type="button" class="mt-1 btn btn-outline-secondary" onclick="history.back();"><i class="fa fa-arrow-circle-left"></i> Kembali</button>
        </div>
    </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset("public/js/menu/{$menu}.js") }}" defer></script>
@endpush