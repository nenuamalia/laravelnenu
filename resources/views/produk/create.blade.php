@extends('layouts.app')

@section('content')
<div class="app-main__inner">
    <div class="card-header">
        <i class="header-icon pe-7s-magic-wand icon-gradient bg-tempting-azure"> </i> {{ $title }}
    </div>             
    <div class="main-card mb-3 card">
        <div class="card-body">
            <form method="POST" action="{{ route("{$menu}.store") }}" autocomplete="off" enctype="multipart/form-data">
                @csrf

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class=""><span class="text-danger">*</span> {{ __('Nama Produk') }}</label>
                            <input name="nama_produk" id="nama_produk" placeholder="Nama Produk here..." type="text" class="form-control @error('nama_produk') is-invalid @enderror" value="{{ old('nama_produk') }}" autofocus required>

                            @error('nama_produk')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="examplePassword" class=""><span class="text-danger">*</span> {{ __('Harga Produk') }}</label>
                            <input name="harga" id="harga" placeholder="Harga Produk here..." type="text" class="priceFormat form-control @error('harga') is-invalid @enderror" value="{{ old('harga') }}" required>

                            @error('harga')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="exampleName" class=""><span class="text-danger">*</span> {{ __('Stock') }}</label>
                            <input id="stock" type="text" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{ old('stock') }}" placeholder="Stok here..." required onkeyup="checkonlynumber(this);">

                            @error('stock')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="exampleName" class=""><span class="text-danger">*</span> {{ __('Gambar Produk') }}</label>
                            <input name="image" id="image" type="file" class="form-control-file @error('image') is-invalid @enderror" required>
                            <small class="form-text text-danger">*Max size : 5Mb <br> *Allowed type: jpeg|jpg|png</small>

                            @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="mt-1 btn btn-outline-alternate" title="Entry"><i class="pe-7s-diskette"></i> Submit</button>
                <button type="button" class="mt-1 btn btn-outline-secondary" onclick="history.back();"><i class="fa fa-arrow-circle-left"></i> Kembali</button>
            </form>
        </div>
    </div>
    </div>
@endsection

@push('scripts')
Input::merge(array_map('trim', Input::all()));

@endpush