@extends('layouts.app')

@section('content')
<div class="app-main__inner">
    <div class="card-header">
        <i class="header-icon pe-7s-magic-wand icon-gradient bg-tempting-azure"> </i> {{ $title }}
    </div> 

    <input id="getDataTable" type="hidden" value="{{ route("{$menu}.getDataTable") }}" />      
    <input id="getIndex" type="hidden" value="{{ route("{$menu}.index") }}" />      
    <input id="getDelete" type="hidden" value="{{ route("{$menu}.store") }}" /> 

    <div class="main-card mb-3 card">
        <div class="card-body">
            @include('layouts.flashmessage')
            
            <a href="{{ route("{$menu}.create") }}" class="mb-2 mr-2 btn-hover-shine btn btn-info"><i class="fa fa-plus"></i> Tambah <?= ucfirst($title) ?></a>
            <table style="width: 100%;" id="tabel" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id</th>
                        <th>Nama Produk</th>
                        <th>Images</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset("public/js/menu/{$menu}.js") }}" defer></script>
@endpush