@extends('layouts.app')

@section('content')
<div class="app-main__inner">
    <div class="card-header">
        <i class="header-icon pe-7s-magic-wand icon-gradient bg-tempting-azure"> </i> {{ $title }}
    </div>             
    <div class="main-card mb-3 card">
        <div class="card-body">
            <form class="">
                <div class="position-relative row form-group"><label for="exampleEmail" class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9"><label class="col-sm-12 col-form-label">: {{ $res->email }} ?></label></div>
                </div>
                <div class="position-relative row form-group"><label for="exampleEmail" class="col-sm-3 col-form-label">No Handphone</label>
                    <div class="col-sm-9"><label class="col-sm-12 col-form-label">: {{ $res->nohp }}</label></div>
                </div>
                <div class="position-relative row form-group"><label for="exampleEmail" class="col-sm-3 col-form-label">Alamat</label>
                    <div class="col-sm-9"><label class="col-sm-12 col-form-label">: {{ $res->alamat }}</label></div>
                </div>
            </form>
             <button type="button" class="mt-1 btn btn-outline-secondary" onclick="history.back();"><i class="fa fa-arrow-circle-left"></i> Kembali</button>
        </div>
    </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset("public/js/menu/{$menu}.js") }}" defer></script>
@endpush