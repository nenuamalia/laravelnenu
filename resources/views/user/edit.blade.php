@extends('layouts.app')

@section('content')
<div class="app-main__inner">
    <div class="card-header">
        <i class="header-icon pe-7s-magic-wand icon-gradient bg-tempting-azure"> </i> {{ $title }}
    </div>             
    <div class="main-card mb-3 card">
        <div class="card-body">
            <form method="POST" action="{{ route("{$menu}.update", $res->id) }}" autocomplete="off">
                @csrf
                @method('PUT')

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="exampleEmail" class=""><span class="text-danger">*</span> {{ __('E-Mail Address') }}</label>
                            <input name="email" id="email" placeholder="Email here..." type="email" class="form-control @error('email') is-invalid @enderror" value="{{ $res->email }}" autofocus required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="exampleName" class=""><span class="text-danger">*</span> {{ __('No Handphone') }}</label>
                            <input id="nohp" type="text" class="form-control @error('nohp') is-invalid @enderror" name="nohp" value="{{ $res->nohp }}" placeholder="No Handphone here..." required autocomplete="name" onkeyup="checkonlynumber(this);">

                            @error('nohp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="examplePassword" class="">{{ __('Password') }}</label>
                            <input name="password" id="password" placeholder="Password here..." type="password" class="form-control @error('password') is-invalid @enderror" autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="examplePasswordRep" class="">{{ __('Repeat Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Repeat Password here..." autocomplete="new-password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="exampleAlamat" class="">{{ __('Alamat') }}</label>
                            <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="10">{{ $res->alamat }}</textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="mt-1 btn btn-outline-alternate" title="Entry"><i class="pe-7s-diskette"></i> Submit</button>
                <button type="button" class="mt-1 btn btn-outline-secondary" onclick="history.back();"><i class="fa fa-arrow-circle-left"></i> Kembali</button>
            </form>
        </div>
    </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset("public/js/menu/{$menu}.js") }}" defer></script>
@endpush