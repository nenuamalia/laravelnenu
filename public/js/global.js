$(document).ready(function () {
    $(".priceFormat").priceFormat({
        prefix: '',
        centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
});

function checkonlynumber(ob) {
    var validChars = /[^0-9-]/gi;
    if (validChars.test(ob.value)) {
        ob.value = ob.value.replace(validChars, "");
    }
}

function checkonlychar(ob) {
    var validChars = /[^a-z\s]/gi;
    if (validChars.test(ob.value)) {
        ob.value = ob.value.replace(validChars, "");
    }
}

function addCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function round(x) {
    return Math.ceil(x);
}
