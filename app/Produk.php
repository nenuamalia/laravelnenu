<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Produk extends Model
{
    use Notifiable;

    protected $table       = "tbl_produk";
    protected $primary_key  = "id";
    protected $fillable     = ['nama_produk', 'image', 'harga', 'stock'];
}
