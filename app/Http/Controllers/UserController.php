<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\Datatables\Datatables;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $menu;

    public function __construct()
    {
        $this->middleware('auth');
        $this->menu = 'user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title  = "Data Users";
        $menu   = $this->menu;
        return view("{$this->menu}.index", compact('title', 'menu'));
    }

    public function getDataTable()
    {
        $data = User::latest()->get();
        return Datatables::of($data)->addIndexColumn()->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title  = "Tambah Users";
        $menu   = $this->menu;
        return view("{$this->menu}.create", compact('title', 'menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'     => ['required', 'string', 'email', 'max:50', 'unique:tbl_user'],
            'password'  => ['required', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/', 'min:6', 'confirmed'],
            'nohp'      => ['required', 'numeric', 'min:8']
        ]);

        User::create($request->all());
        return redirect()->route("{$this->menu}.index")->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res    = User::findOrFail($id);
        $title  = "Users Detail";
        $menu   = $this->menu;
        return view("{$this->menu}.show", compact('res', 'title', 'menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res    = User::findOrFail($id);
        $title  = "Edit Users";
        $menu   = $this->menu;
        return view("{$this->menu}.edit", compact('res', 'title', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email'     => ['required', 'string', 'email', 'max:50'],
            'nohp'      => ['required', 'numeric', 'min:8']
        ]);

        $data = User::findOrFail($id);
        if (trim($request->password) == '') {
            $input = $request->except('password');
        } else {
            $request->validate([
                'password'  => ['regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/', 'min:6', 'confirmed']
            ]);

            $input = $request->all();
            $input['password'] = Hash::make($request['password']);
        }

        $data->update($input);
        return redirect()->route("{$this->menu}.index")->with('success', 'User saved successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();

        return response()->json(['success' => 'Product saved successfully.']);
    }
}
