<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Produk;

class ProdukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $menu;

    public function __construct()
    {
        $this->middleware('auth');
        $this->menu = 'produk';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title  = "Data Produk";
        $menu   = $this->menu;
        return view("{$this->menu}.index", compact('title', 'menu'));
    }

    public function getDataTable()
    {
        $data = Produk::latest()->get();
        return Datatables::of($data)->addIndexColumn()
            ->addColumn('image', function ($row) {
                $return = "<img src='" . asset("public/images/produk/{$row->image}") . "' alt='user' width='auto' height='100' />";
                return $return;
            })
            ->addColumn('harga', function ($row) {
                $return = number_format($row->harga, 0, ',', '.');
                return $return;
            })->addColumn('stock', function ($row) {
                $return = number_format($row->stock, 0, ',', '.');
                return $return;
            })->rawColumns(['image', 'harga', 'stock'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title  = "Tambah Produk";
        $menu   = $this->menu;
        return view("{$this->menu}.create", compact('title', 'menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama_produk'   => ['required', 'string', 'unique:tbl_produk'],
            'image'         => ['required', 'image', 'mimes:jpg,png,jpeg', 'max:5120'],
            'harga'         => ['required'],
            'stock'         => ['required', 'numeric'],
        ]);

        $input = $request->all();
        $input['harga'] = str_replace(".", "", $request['harga']);

        if ($file = $request->file('image')) {
            $image = time() . $file->getClientOriginalName();
            $file->move(public_path('images\\produk'), $image);

            $input['image'] = $image;
        }

        Produk::create($input);
        return redirect()->route("{$this->menu}.index")->with('success', 'Produk created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res    = Produk::findOrFail($id);
        $title  = "Produk Detail";
        $menu   = $this->menu;
        return view("{$this->menu}.show", compact('res', 'title', 'menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res    = Produk::findOrFail($id);
        $title  = "Edit Produk";
        $menu   = $this->menu;
        return view("{$this->menu}.edit", compact('res', 'title', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_produk'   => ['required', 'string'],
            'harga'         => ['required'],
            'stock'         => ['required', 'numeric'],
        ]);

        $data = Produk::findOrFail($id);

        $input = $request->all();

        if (empty($request->image)) {
            $input = $request->except('image');
            $input['harga'] = str_replace(".", "", $request['harga']);
        } else {
            if ($file = $request->file('image')) {
                $request->validate([
                    'image'         => ['required', 'image', 'mimes:jpg,png,jpeg', 'max:5120']
                ]);

                unlink(public_path('images\\produk\\') . $data->image);

                $image = time() . $file->getClientOriginalName();
                $file->move(public_path('images\\produk'), $image);

                $input['image'] = $image;
            }
        }
        
        $data->update($input);
        return redirect()->route("{$this->menu}.index")->with('success', 'Produk saved successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Produk::findOrFail($id);

        unlink(public_path('images\\produk\\') . $data->image);

        $data->delete();
        return response()->json(['success' => 'Product has been deleted']);
    }
}
