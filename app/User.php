<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    use Notifiable;

    protected $table       = "tbl_user";
    protected $primary_key  = "id";
    protected $fillable     = ['email', 'password', 'nohp', 'alamat'];
    protected $hidden       = ['password', 'remember_token'];
    protected $casts        = ['email_verified_at' => 'datetime'];
}
